## Question 
[insert question]]
## Source
[insert question source]]

## Audience & Location - which FAQ to use?
- [ ] visitor - on website
- [ ] user - in user documentation
- [ ] contributor - in devel documentation

## Answer draft
[insert answer draft here]

## Validation workflow
- [ ] identify question
- [ ] identify source
- [ ] verify duplication in faq.md
- [ ] draft answer
- [ ] get feedback from team
- [ ] validate by Morane / Benoît / Roberto (depends on scope)
